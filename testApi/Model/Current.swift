//
//  Current.swift
//  testApi
//
//  Created by Дмитрий on 27.07.2023.
//
import Foundation

struct Location: Decodable {
    let name: String
    let region: String
    let country: String
    
    init (data: [String: Any]) {
        name = data["name"] as? String ?? ""
        region = data["region"] as? String ?? ""
        country = data["country"] as? String ?? ""
    }
}

struct Current: Decodable {
    let condition: Condition
    let tempC: Double
    
    init (data: [String:Any]) {
        guard let conditionData = data["condition"] as? [String: Any] else {
            fatalError("Failed to parse Condition")
        }
        
        tempC = data["temp_c"] as? Double ?? 0.0
        condition = Condition(data: conditionData)
    }
}

struct Condition: Decodable {
    let text: String
    let icon: String
    
    init (data: [String:Any]) {
        text = data["text"] as? String ?? ""
        icon = data["icon"] as? String ?? ""
    }
}

struct Weather: Decodable {
    let location: Location
    let current: Current
    
    init(data: [String: Any]) {
        guard let locationData = data["location"] as? [String: Any],
              let currentData = data["current"] as? [String: Any] else {
            fatalError("Failed to parse Condition")
        }
        
        location = Location(data: locationData)
        current = Current(data: currentData)
        
    }
    
    static func getWeather(from value: Any) -> Weather? {
        guard let weatherData = value as? [String: Any] else { return nil }
        return Weather(data: weatherData)
    }
}

extension Current {
    enum CodingKeys: String, CodingKey {
        case condition = "condition"
        case tempC = "temp_c"
    }
}

struct City {
    let url: String
    let name: String
}

