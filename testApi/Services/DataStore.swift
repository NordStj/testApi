//
//  DataStore.swift
//  testApi
//
//  Created by Дмитрий on 27.07.2023.
//

import Foundation
import Alamofire

enum NetworkError: Error {
    case invalidURL
    case noData
    case decodingError
}

final class DataManager {
    
    static let shared = DataManager()
    
    var linkCity: [(String, String)] = [
        ("https://api.weatherapi.com/v1/current.json?key=14a92bb7e2f04d78af291745232707&q=Moscow&aqi=yes", "Moscow"),
        ("https://api.weatherapi.com/v1/current.json?key=14a92bb7e2f04d78af291745232707&q=London&aqi=yes", "London"),
        ("https://api.weatherapi.com/v1/current.json?key=14a92bb7e2f04d78af291745232707&q=New%20York&aqi=yes", "New York"),
        ("https://api.weatherapi.com/v1/current.json?key=14a92bb7e2f04d78af291745232707&q=Pekin&aqi=yes", "Pekin")
    ]
    
    private init() {}
    
    func addCity(city: City) {
        if linkCity.contains(where: { $0.1 == city.name }) {
            print("city not added")
            return
        } else {
            linkCity.append((city.url, city.name))
        }
    }
    
}

final class NetworkManager {
    
    static let shared = NetworkManager()
        
    private init () {}
    
    func fetch(from url: String, completion: @escaping(Result<Weather, AFError>) -> Void) {
        AF.request(url)
            .validate()
            .responseJSON { dataResponse in
                switch dataResponse.result {
                case .success(let value):
                    guard let weather = Weather.getWeather(from: value) else {
                        return
                    }
                    completion(.success(weather))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
    }
    
    func fetchData(from url: String, completion: @escaping(Result<Data, AFError>) -> Void) {
        AF.request(url)
            .validate()
            .responseData { dataResponse in
                switch dataResponse.result {
                case .success(let imageData):
                    completion(.success(imageData))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
    }
}


