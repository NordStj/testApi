//
//  WeatherCell.swift
//  testApi
//
//  Created by Дмитрий on 28.07.2023.
//

import UIKit

final class WeatherCell: UITableViewCell {
    
    @IBOutlet var ImageView: UIImageView!
    @IBOutlet var cityLabel: UILabel!
    @IBOutlet var tempLabel: UILabel!
    @IBOutlet var infoLabel: UILabel!
    @IBOutlet var countryLabel: UILabel!
    
    
    private let networkManager = NetworkManager.shared
    
    func configureCell(with city: (String, String)) {
        
        cityLabel.text = city.1
        let url = city.0
        
        networkManager.fetch(from: url) { [weak self] results in
            switch results {
            case .success(let data):
                DispatchQueue.main.async {
                    self?.tempLabel.text = String(format: "%.0f°", data.current.tempC)
                    self?.infoLabel.text = String(data.current.condition.text)
                    self?.countryLabel.text = String(data.location.country)
                }
                self?.configureImage(url: "https:\(data.current.condition.icon)")
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func configureImage(url: String) {
        networkManager.fetchData(from: url) { [weak self] result in
            switch result {
            case .success(let data):
                DispatchQueue.main.async {
                    self?.ImageView.image = UIImage(data: data)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}
