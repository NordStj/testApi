//
//  WeathersViewController.swift
//  testApi
//
//  Created by Дмитрий on 28.07.2023.
//

import UIKit

final class WeathersViewController: UITableViewController {
    
    private var dataManager = DataManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = 160
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
}
    // MARK: - Table view data source

extension WeathersViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataManager.linkCity.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        guard let cell = cell as? WeatherCell else { return UITableViewCell() }
        let city = dataManager.linkCity[indexPath.row]
        cell.configureCell(with: city)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
