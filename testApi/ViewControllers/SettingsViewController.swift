//
//  SettingsViewController.swift
//  testApi
//
//  Created by Дмитрий on 28.07.2023.
//

import UIKit

final class SettingsViewController: UIViewController {
    
    @IBOutlet var findTF: UITextField!
    @IBOutlet var findSV: UIStackView!
    @IBOutlet var findButton: UIButton!
    @IBOutlet var addButton: UIButton!
    @IBOutlet var resultLabel: UILabel!
    
    @IBOutlet var findImage: UIImageView!
    @IBOutlet var cityLabel: UILabel!
    @IBOutlet var countryLabel: UILabel!
    @IBOutlet var tempLabel: UILabel!
    
    private var networkManager = NetworkManager.shared
    private var dataManager = DataManager.shared
    
    private var newCity =  City(url: "", name: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switchInterfaice(bool: true)
        switchResult(bool: true, result: "")
        
        findButton.layer.cornerRadius = 15
        addButton.layer.cornerRadius = 15
    }
    
    @IBAction func findButton(_ sender: Any) {
        
        switchResult(bool: true, result: "")
        switchInterfaice(bool: true)
        
        guard let findCity = findTF.text else {return}
        guard let url = URL(string:"https://api.weatherapi.com/v1/current.json?key=14a92bb7e2f04d78af291745232707&q=\(findCity)&aqi=yes") else {
            switchResult(bool: false, result: "City not found")
            return
        }
        fetchData(url: url)
    }
    
    @IBAction func useAddCity(_ sender: Any) {
        dataManager.addCity(city: newCity)
        switchInterfaice(bool: true)
        switchResult(bool: false, result: "Operation completed")
    }
}

extension SettingsViewController {
    
    private func fetchData(url: URL) {
        networkManager.fetch(from: url.absoluteString) { [weak self] result in
            switch result {
            case .success(let data):
                DispatchQueue.main.async {
                    self?.switchInterfaice(bool: false)
                    self?.cityLabel.text = String(data.location.name)
                    self?.tempLabel.text = String(format: "%.0f°", data.current.tempC)
                    self?.countryLabel.text = String(data.location.country)
                    self?.newCity = City(url: url.absoluteString, name: data.location.name)
                }
                self?.configureImage(url: "https:\(data.current.condition.icon)")
            case .failure(let error):
                DispatchQueue.main.async {
                    self?.switchResult(bool: false, result: "City not found")
                }
                print(error)
            }
        }
    }
    
    private func configureImage(url: String) {
        networkManager.fetchData(from: url) { [weak self] result in
            switch result {
            case .success(let data):
                DispatchQueue.main.async {
                    self?.findImage.image = UIImage(data: data)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func switchInterfaice(bool: Bool) {
            findSV.isHidden = bool
            addButton.isHidden = bool
    }
    
    
    private func switchResult(bool: Bool, result: String) {
        resultLabel.isHidden = bool
        if !bool {
            resultLabel.text = result
        }
    }
}
